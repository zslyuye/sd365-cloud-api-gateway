package com.sd365.gateway.authentication.api.controller;

import com.alibaba.fastjson.JSONObject;
import com.sd365.common.util.TokenUtil;
import com.sd365.gateway.authentication.api.AuthenticationApi;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;
import java.util.LinkedHashMap;

/**
 * @author yanduohuang
 * @version 1.0
 * @date 2020/12/17 20:57
 * @className
 * @description
 */
@Slf4j
@RestController
public class AuthenticationController implements AuthenticationApi {

    @Autowired
    private RestTemplate restTemplate;

    @Override
    public String getToken(String code, String account, String password) {
        log.info(String.format("getToken call %s %s",code,account));
        String url = "http://gps-permission-center/permission/centre/vue-admin-template/user/auth?code=%s&account=%s&password=%s";
        url = String.format(url,code,account,password);
        LinkedHashMap forObject=null;
        try {
            forObject = restTemplate.getForObject(url, LinkedHashMap.class);
        }catch (Exception ex){
            log.info("restTemplate.getForObject error",ex);
            throw ex;
        }

        Integer respCode = null;
        ArrayList roleIds = null;
        Long tenantId = null;
        Long companyId = null;
        Long orgId = null;
        final LinkedHashMap body = (LinkedHashMap) forObject.get("body");
        try {

            respCode = (Integer) (body).get("code");
            roleIds = (ArrayList)(body).get("roleIds");
            tenantId = Long.parseLong((String)body.getOrDefault("tenantId","-1"));
            companyId = Long.parseLong((String)body.getOrDefault("companyId","-1"));
            orgId = Long.parseLong((String)body.getOrDefault("orgId","-1"));
        } catch (Exception e) {
            throw e;
        }
        if (respCode != 200) {
            return new JSONObject(forObject).toJSONString();
        }

        final JSONObject header = new JSONObject();
        final JSONObject playLoad = new JSONObject();
        header.put("alg","HS256");
        header.put("typ","JWT");
        playLoad.put("account", account);
        playLoad.put("roleIds", roleIds);
        playLoad.put("tenantId", tenantId);
        playLoad.put("companyId", companyId);
        playLoad.put("orgId", orgId);
        String token = header.toJSONString()+"."+playLoad.toJSONString();
        ((LinkedHashMap) (body).get("data")).put("token", TokenUtil.encoderToken(token));
        return new JSONObject(forObject).toJSONString();
    }
}
