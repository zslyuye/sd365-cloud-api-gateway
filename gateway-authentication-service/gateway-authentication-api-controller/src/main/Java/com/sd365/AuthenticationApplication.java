package com.sd365;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author yanduohuang
 * @version 1.0
 * @date 2020/12/17 21:02
 * @className
 * @description
 */
@SpringBootApplication
@EnableDiscoveryClient
@EnableSwagger2
//添加熔断降级注解
@EnableCircuitBreaker
public class AuthenticationApplication {

    public static void main(String[] args) {
        SpringApplication.run(AuthenticationApplication.class, args);
    }
}
