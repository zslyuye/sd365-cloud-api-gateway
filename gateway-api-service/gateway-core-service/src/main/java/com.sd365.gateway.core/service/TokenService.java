package com.sd365.gateway.core.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;

@FeignClient(value = "gateway-authorization")
public interface TokenService {
    @GetMapping(value = "/v1/authorization")
    public Boolean authorization(@RequestParam("token") String token, @RequestParam("url") String url);
}
