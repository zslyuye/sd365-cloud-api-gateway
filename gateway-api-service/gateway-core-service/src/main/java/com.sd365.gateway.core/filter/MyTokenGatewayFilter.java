package com.sd365.gateway.core.filter;

import com.alibaba.nacos.client.utils.StringUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cloud.gateway.filter.GatewayFilterChain;
import org.springframework.cloud.gateway.filter.GlobalFilter;
import org.springframework.core.Ordered;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.server.ServerWebExchange;
import reactor.core.publisher.Mono;

import javax.annotation.Resource;
import java.util.Map;

/**
 * @class MyTokenGatewayFilter
 * @classdesc 判断Token是否存在，转发到授权中心
 * @author zhengjianjian
 * @date 2020/12/18
 * @version 1.0.0
 * @see
 * @since
 */
@Slf4j
@Component
public class MyTokenGatewayFilter implements GlobalFilter, Ordered {
    @Resource
    private RestTemplate restTemplate;
    public static final String authorization_URL = "http://gateway-authorization";
    @Override
    public Mono<Void> filter(ServerWebExchange exchange, GatewayFilterChain chain) {
        log.info("进入全局过滤器");
        //获取请求路径
        String URI = exchange.getRequest().getURI().toString();
        log.info("请求地址"+URI+exchange);
        Map<String, Object> attributes = exchange.getAttributes();
        System.out.println("参数值开始打印");
        for (String key:attributes.keySet()){
            String value=attributes.get(key).toString();
            System.out.println(key+"="+value);
        }
        //是否鉴权成功
        Boolean flag = false;
        if(URI.contains("user/login")
                || URI.contains("user/info")
                || URI.contains("user/logout")){
            return chain.filter(exchange);
        }
        //获取Token
//        String Token = exchange.getRequest().getHeaders().get("accessToken").toString();
        String Token = "1";
        log.info(Token);
        if(!StringUtils.isEmpty(Token) || URI.contains("procurement")){
            try {
                log.info("发起restTemplate请求");
                flag =true;
               // flag = restTemplate.getForObject(authorization_URL+"/v1/authorization?token="+Token+"&url="+URI,Boolean.class);
                log.info("restTemplate请求结束");
            }catch (Exception ex){
                log.info("请求异常",ex);
            }

            if(flag){
                return chain.filter(exchange);
            }else{
                exchange.getResponse().setStatusCode(HttpStatus.NOT_ACCEPTABLE);//回应
                return exchange.getResponse().setComplete();
            }

        }
        return chain.filter(exchange);
    }

    @Override
    public int getOrder() {
        return 0;
    }
}
