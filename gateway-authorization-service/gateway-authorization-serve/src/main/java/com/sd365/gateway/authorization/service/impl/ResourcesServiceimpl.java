package com.sd365.gateway.authorization.service.impl;

import com.sd365.gateway.authorization.service.ResourcesService;
import com.sd365.gateway.authorization.dao.mapper.ResourceMapper;
import com.sd365.gateway.authorization.entity.Resource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.List;

/**
 * @Author 吴剑鸣
 * @Date 2020/12/18 12:52
 * @Version 1.0
 * @description
 */
@Service
public class ResourcesServiceimpl implements ResourcesService {
    @Autowired
    ResourceMapper resourceMapper;


    @Override
    public List<Resource> searchResource(List<Long> ids) {
        Example example = new Example(Resource.class);
        example.createCriteria().andIn("id", ids);

        return resourceMapper.selectByExample(example);
    }


}
