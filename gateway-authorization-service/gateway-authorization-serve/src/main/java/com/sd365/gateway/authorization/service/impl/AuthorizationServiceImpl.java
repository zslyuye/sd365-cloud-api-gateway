package com.sd365.gateway.authorization.service.impl;




import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.sd365.gateway.authorization.service.ResourcesService;
import com.sd365.gateway.authorization.entity.Resource;
import com.sd365.gateway.authorization.service.AuthorizationService;
import com.sd365.gateway.authorization.service.RoleResourcesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Base64;
import java.util.LinkedList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.util.regex.Pattern.*;

/**
 * @class WayBillServiceImpl
 * @classdesc
 * @author Administrator
 * @date 2020-10-2  18:04
 * @version 1.0.0
 * @see
 * @since
 */
@Service
public class AuthorizationServiceImpl implements AuthorizationService {
    @Autowired
    ResourcesService resourcesService;
    @Autowired
    RoleResourcesService roleResourcesService;

    @Override
    public Boolean roleAuthorization(String token,String url) {

        List<Long> roleIds = new LinkedList<>();

        /**
         * 解析token获取roleIds
         */
        token = token.split("\\.")[1];
        token = token.substring(0,token.length()-1);

        String tokenCode = new String(Base64.getDecoder().decode(token.getBytes()));

        JSONObject tokenJson = JSONObject.parseObject(tokenCode);
        JSONArray roleIdsJson = (JSONArray)tokenJson.get((Object)"roleIds");
        List<String> roleIdsstr = JSONArray.toJavaObject(roleIdsJson,List.class);

        for( String roleIdstr:roleIdsstr
             ) {
            roleIds.add(Long.parseLong(roleIdstr));
        }

        /**
         * 提取url
         */

        final Pattern compile = compile("^https?:\\/\\/(?:[0-9a-zA-Z:\\.]*)([^\\?]*)");
        Matcher matcher = compile.matcher(url);
        if(matcher.find()){
            url = matcher.group(1);
        }

        /**
         * 查表鉴权
         */

        List<Long> resourceIds = roleResourcesService.searchResourceIdsByroleIds(roleIds);
        if(resourceIds == null || resourceIds.size()==0) {
            return false;
        }

        List<Resource> resources = resourcesService.searchResource(resourceIds);
        for (Resource resource:resources){
            if(resource.getApi() != null){
                final Pattern pattern = compile(String.format("%s$", resource.getApi()));
                final Matcher matcherApi = pattern.matcher(url);
                if (matcherApi.find()) {
                    return true;
                }
            }
        }
        return false;
    }
}
