package com.sd365.gateway.authorization.service;

import com.sd365.gateway.authorization.entity.Resource;

import java.util.List;

/**
 * @Author 吴剑鸣
 * @Date 2020/12/18 12:50
 * @Version 1.0
 * @description
 */
public interface ResourcesService {

    List<Resource> searchResource(List<Long> ids);
}
