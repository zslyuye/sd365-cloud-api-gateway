package com.sd365.gateway.authorization.service;


/**
 * @class
 * @classdesc
 * @author
 * @date 2020-10-2  17:58
 * @version 1.0.0
 * @see
 * @since
 */
public interface AuthorizationService {

    /**
     *
     * @param token
     * @return
     */
    Boolean roleAuthorization(String token,String uri);

}
