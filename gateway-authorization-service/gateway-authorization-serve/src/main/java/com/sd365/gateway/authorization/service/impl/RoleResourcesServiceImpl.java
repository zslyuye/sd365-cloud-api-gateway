package com.sd365.gateway.authorization.service.impl;

import com.sd365.gateway.authorization.service.RoleResourcesService;
import com.sd365.gateway.authorization.dao.mapper.RoleResourceMapper;
import com.sd365.gateway.authorization.entity.RoleResource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import tk.mybatis.mapper.entity.Example;

import java.util.*;

/**
 * @Author 吴剑鸣
 * @Date 2020/12/18 12:53
 * @Version 1.0
 * @description
 */
@Service
public class RoleResourcesServiceImpl implements RoleResourcesService {
    @Autowired
    RoleResourceMapper roleResourceMapper;

    @Override
    public List<Long> searchResourceIdsByroleIds(List<Long> roleIds) {


        Example example = new Example(RoleResource.class);
        example.createCriteria().andIn("roleId", roleIds);
        List<RoleResource> roleResources = roleResourceMapper.selectByExample(example);
        List<Long> resourceIds = new LinkedList<>();
        for (RoleResource roleResource: roleResources
             ) {
            resourceIds.add(roleResource.getResourceId());
        }
        return  resourceIds;
    }



}
