package com.sd365.gateway.authorization.dao.mapper;

import com.sd365.gateway.authorization.entity.RoleResource;
import tk.mybatis.mapper.common.Mapper;

public interface RoleResourceMapper extends Mapper<RoleResource> {

}