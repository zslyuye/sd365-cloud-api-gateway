package com.sd365.gateway.authorization.dao.mapper;

import com.sd365.gateway.authorization.entity.Resource;
import tk.mybatis.mapper.common.Mapper;

public interface ResourceMapper extends Mapper<Resource> {
}