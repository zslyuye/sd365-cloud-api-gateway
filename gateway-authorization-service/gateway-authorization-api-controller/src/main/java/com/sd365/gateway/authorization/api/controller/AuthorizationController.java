package com.sd365.gateway.authorization.api.controller;

import com.sd365.common.core.common.controller.AbstractController;
import com.sd365.common.core.context.BaseContextHolder;
import com.sd365.gateway.authorization.api.Authorization;
import com.sd365.gateway.authorization.service.AuthorizationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;


/**
 * @class
 * @classdesc
 * @author
 * @date
 * @version 1.0.0
 * @see
 * @since
 */
//@RestController
@Controller
public class AuthorizationController extends AbstractController implements Authorization {
    @Autowired
    AuthorizationService authorizationService;

    @Override
    public Boolean roleAuthorization(String token,String url) {
        BaseContextHolder.set("nowrap",new Object());
        return authorizationService.roleAuthorization(token,url);
    }
}
