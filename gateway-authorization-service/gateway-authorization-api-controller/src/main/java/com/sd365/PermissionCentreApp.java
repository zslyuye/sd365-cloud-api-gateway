package com.sd365;
import org.springframework.amqp.rabbit.annotation.EnableRabbit;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.client.discovery.EnableDiscoveryClient;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import springfox.documentation.swagger2.annotations.EnableSwagger2;
import tk.mybatis.spring.annotation.MapperScan;

@SpringBootApplication
@ComponentScans({
        @ComponentScan("com.sd365.common"),
        @ComponentScan("com.sd365.common.api.version")
})
@MapperScan(("com.sd365.gateway.authorization.dao.mapper"))
//开启注解支持
@EnableCaching
@EnableSwagger2
@EnableDiscoveryClient
@EnableRabbit
public class PermissionCentreApp
{
    public static void main( String[] args )
    {
        SpringApplication.run(PermissionCentreApp.class, args);
    }
}
