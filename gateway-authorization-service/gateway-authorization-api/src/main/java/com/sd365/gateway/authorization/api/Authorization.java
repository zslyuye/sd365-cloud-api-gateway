package com.sd365.gateway.authorization.api;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * @class
 * @classdesc
 * @author Administrator
 * @date 2020-10-2  23:16
 * @version 1.0.0
 * @see
 * @since
 */
@RequestMapping(value = "/v1")
public interface Authorization {
    /**
     * @param:
     * @return:
     * @see
     * @since
     */
    @GetMapping(value = "/authorization")
    @ResponseBody
    Boolean roleAuthorization(String token,String url);

}
